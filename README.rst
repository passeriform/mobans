coala-mobans
============= 
 
coala-moban is a collection of useful templates that are mainly used
by coala to have standardized configuration over all of the coala 
repositories.

These templates are applied using ``moban`` templating engine.

`Template Engine <https://en.wikipedia.org/wiki/Template_processor>`__ 

Usage
~~~~~

Syncing a coala repo
---------------------

In order to apply templates of this repo to any of the coala repository :

1. Create a parent folder with coala repos and coala-moban as subdirectory,
`i.e`, the structure should be:

::

    parent-directory
    ├── <coala-repo>
    ├── coala-mobans


2. Clone the coala-mobans repo.

    ``$ git clone https://gitlab.com/coala/mobans coala-mobans``

3. Create a ``virtualenv`` and install all dependencies from
   ``test-requirements.txt``.

4. **cd** into the root folder of any coala repository and run :

     ``$ moban``
     
This will automatically make all the necessary changes as mentioned in
the template files.

**Precaution**: The ``mobans`` repo should be named as ``coala-mobans``. This
is done automatically while cloning in the above format.


Syncing moban repo
-------------------

To sync the moban repository:


1. **cd** into the mobans repository and inorder to pull in the changes run :

     ``$ moban``


moban
~~~~~~~~~~
moban is a cli command tool which uses the high performance template 
engine (JINJA2) for static text generation.

`moban <http://moban.readthedocs.io/en/latest/>`__ 

Jinja2
~~~~~~
Jinja2 is a full featured template engine for Python. It has full unicode 
support, an optional integrated sandboxed execution environment, widely 
used and BSD licensed. It is similar to the Django template engine.

`Jinja2 <http://jinja.pocoo.org/docs/2.10/>`__
 
Windows
~~~~~~~

The Windows CI and developer environment is centrally controlled here by
[`Fudgefile`](Fudgefile), which is used by https://github.com/Badgerati/Fudge .

It ensures that all coala dependencies and components are installed,
and that the .coafile in each repository can be run.

This is generated from `coala-ci-requirements.yaml`, which is a distilled
from the `pm-requirements.yaml` obtained from
https://gitlab.com/coala/package_manager/ , with most of the unnecessary
components filtered out.  Major improvements to the Windows environment should
be done at https://gitlab.com/coala/package_manager/ , although some files
contain coala logic for multiple repositories, such as
`assets/fudge/deps.python-packages.ps1` which should be enhanced in this
mobans repository.

The CI comes in two flavours: With and Without Fudge.

The minimal version only works on AppVeyor, and can be seen at PyPrints and
coala-utils, which have few dependencies.
It requires only three entries in `.moban.yaml`:

    - .ci/constants.ps1: constants.ps1.jj2
    - .ci/deps.python-packages.ps1: fudge/deps.python-packages.ps1
    - .ci/appveyor.yml: ci/appveyor.yml.jj2

However for core coala repositories with more complicated dependencies, the
following are also needed, allows Windows builds on Travis CI, and developers
to install everything locally.  It requires these additional entries:

    - Fudgefile: Fudgefile.jj2
    - .ci/Fudge.ps1: fudge/Fudge.ps1
    - .ci/Modules/FudgeTools.psm1: fudge/Modules/FudgeTools.psm1
    - .ci/FudgeCI.ps1: fudge/FudgeCI.ps1
    - .ci/FudgeGenerateFake.ps1: fudge/FudgeGenerateFake.ps1
    - .ci/PrepareAVVM.ps1: fudge/PrepareAVVM.ps1
    - .ci/FudgePostInstall.ps1: fudge/FudgePostInstall.ps1
  - .ci/store_env_in_registry.py: ci/store_env_in_registry.py

Also `.moban.yaml` should override `coala-ci-requirements.yaml`, usually
replacing `overrides: coala.yaml` at the beginning of the file.

The Windows CI carefully avoids any dependency on on Visual Studio.
Implementation of Visual Studio support should occur at
https://gitlab.com/coala/package_manager/

In addition, the Travis Windows CI needs one extra helper:

    - .ci/refreshenv.sh: ci/refreshenv.sh

Once the Fudgefile exists in the repository, developers can install
and run Fudge, or use the locally stored copy in .ci/Fudge.ps1

    - powershell -c ". .ci/Fudge.ps1 install"

Any repository which has modifications to Powershell files also needs the
custom linter and linter rules.

  - .ci/PSLint.ps1: ci/PSLint.ps1
  - .ci/Export-NUnitXml.psm1: ci/Export-NUnitXml.psm1
  - .ci/PSScriptAnalyzerSettings.psd1: ci/PSScriptAnalyzerSettings.psd1
